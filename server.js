const express = require('express');
const next = require('next');
const fetch = require('isomorphic-fetch');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handler = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();    

    server.get('/p/:id', async (req, res) => {
      const actualPages = '/post';
      
      const result = await fetch(`https://api.tvmaze.com/shows/${req.params.id}`);
      const show = await result.json();

      const queryParams = { 
        title: req.params.id,
        show,
      };

      app.render(req, res, actualPages, queryParams);
    })

    server.get('*', (req, res) => {
      return handler(req, res)
    });

    server.listen(3000, (err) => {
      if (err) throw err;

      console.log(' > Ready on http://localhost:3000');
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  })