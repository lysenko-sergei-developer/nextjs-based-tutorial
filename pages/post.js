import Layout from '../comps/MyLayout';
import fetch from 'isomorphic-fetch';

const Post = (props) => {

  const show = props.url.query.show ? props.url.query.show : props.show;

  return (
    <div>
    <Layout>
      <h1>{show.name}</h1>

      <p>{show.summary.replace(/<[/]?\p\b>/g, '')}</p>
      <img src={show.image.medium}/>
      <style jsx global>{`
        h1, p {
          font-family: "Arial";
        }

        a {
          text-decoration: none;
          color: blue;
        }
      `}</style>
    </Layout>
  </div>
  )
}

Post.getInitialProps = async (context) => {
  const { id } = context.query

  const res = await fetch(`https://api.tvmaze.com/shows/${id}`)
  const show = await res.json()

  console.log(`Fetched show: ${show.name}`);

  return { 
    show
  }
}

export default Post;