import Layout from '../comps/MyLayout';
import Link from 'next/link';
import fetch from 'isomorphic-fetch';

const PostLink = (props) => <li>
  <Link prefetch as={`/p/${props.id}`} href={`post?id=${props.id}`}>
    <a>{props.name}</a>
  </Link>
  <style jsx>{`
      h1, a {
        font-family: "Arial";
      }

      ul {
        padding: 0;
      }

      li {
        list-style: none;
        margin: 5px 0;
      }

      a {
        text-decoration: none;
        color: blue;
      }

      a:hover {
        opacity: 0.6;
      }
    `}</style>
</li>

const Index = (props) => <div>
  <Layout>
    <h1>Batman TV Shows</h1>
    <ul>
      {
        props.shows.map(({show}) => 
          <PostLink key={show.id} name={show.name} id={show.id} />)
      }
    </ul>
    <style jsx>{`
      h1, a {
        font-family: "Arial";
      }

      ul {
        padding: 0;
      }

      li {
        list-style: none;
        margin: 5px 0;
      }

      a {
        text-decoration: none;
        color: blue;
      }

      a:hover {
        opacity: 0.6;
      }
    `}</style>
  </Layout>
</div>

Index.getInitialProps = async () => {
  const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
  const data = await res.json();

  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    shows: data,
  }
}

export default Index;