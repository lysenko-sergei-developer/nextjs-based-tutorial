import Layout from '../comps/MyLayout';

const About = () => <div>
  <Layout>
    <h1>About me</h1>
    <h2>Hello! My name Sergey. I'm twenty-one years old.</h2>
  </Layout>
</div>

export default About;